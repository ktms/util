#ifndef __MEMPOOL_HDR_
#define __MEMPOOL_HDR_

namespace mempool
{
	void* allocate(size_t size, int align);

	void free(void *ptr, size_t size = 1);
}


template <typename object_type, typename ...arg>
object_type* new_object(arg... params)
{
	return new (mempool::allocate(sizeof(object_type), 1)) object_type(params...);
}

template <typename object_type>
void delete_object(object_type* object)
{
	object->~object_type();
	mempool::free(object);
}

#endif /*__MEMPOOL_HDR_*/
