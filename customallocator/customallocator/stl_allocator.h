#ifndef __MY_STL_ALLOCATOR_
#define __MY_STL_ALLOCATOR_

#include <cstddef>
#include "mempool.h"

template<typename T>
class Allocator
{
public:
	typedef size_t		size_type;
	typedef ptrdiff_t	difference_type;
	typedef T*			pointer;
	typedef const T*	const_pointer;
	typedef T&			reference;
	typedef const T&	const_reference;
	typedef T			value_type;

	/// Default constructor
	Allocator() throw()
	{
	}
	/// Copy constructor
	Allocator(const Allocator&) throw()
	{
	}
	/// Copy constructor with another type
	template<typename U>
	Allocator(const Allocator<U>&) throw()
	{
	}

	/// Destructor
	~Allocator()
	{
	}

	/// Copy
	Allocator<T>& operator=(const Allocator&)
	{
		return *this;
	}
	/// Copy with another type
	template<typename U>
	Allocator& operator=(const Allocator<U>&)
	{
		return *this;
	}

	/// Get address of reference
	pointer address(reference x) const
	{
		return &x;
	}
	/// Get const address of const reference
	const_pointer address(const_reference x) const
	{
		return &x;
	}

	/// Allocate memory
	pointer allocate(size_type n, const void* = 0)
	{
		size_type size = n * sizeof(value_type);
		return (pointer)mempool::allocate(size, 1);
	}

	/// Deallocate memory
	void deallocate(void* p, size_type n)
	{
		size_type size = n * sizeof(T);
		mempool::free(p, size);
	}

	/// Call constructor
	void construct(pointer p, const T& val)
	{
		// Placement new
		new ((T*)p) T(val);
	}
	/// Call constructor with more arguments
	template<typename U, typename... Args>
	void construct(U* p, Args&&... args)
	{
		// Placement new
		::new((void*)p) U(std::forward<Args>(args)...);
	}

	/// Call the destructor of p
	void destroy(pointer p)
	{
		p->~T();
	}
	/// Call the destructor of p of type U
	template<typename U>
	void destroy(U* p)
	{
		p->~U();
	}

	/// Get the max allocation size
	size_type max_size() const
	{
		return size_type(-1);
	}

	/// A struct to rebind the allocator to another allocator of type U
	template<typename U>
	struct rebind
	{
		typedef Allocator<U> other;
	};
};

#endif /*__MY_STL_ALLOCATOR_*/
