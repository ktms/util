#include <malloc.h>
#include <assert.h>
#include "mempool.h"
#include "objectpool.h"
#include <stdio.h>



/*
NO SUPPORT FOR CUSTOM ALIGMENT FOR NOW!
*/

template <int SIZE_IN_BYTES>
struct MemoryBucket
{
	MemoryBucket()
	{
		size	= SIZE_IN_BYTES;
		align	= 1;
		used	= false;
	}

	void* getAddress()
	{
		return (void *)memory;
	}

	char				memory[SIZE_IN_BYTES];
	size_t				size;
	int					align;
	bool				used;
};

class Mempool
{
	protected:

		BucketPool< MemoryBucket<64>, 2048>		smallPool;	// for 2048 allocated objects with size <= 64 bytes

	public:

		void* allocate(size_t size, int align)
		{
			return (void *)smallPool.alloc()->memory;
		}

		void free(void *ptr, size_t size)
		{
			smallPool.freeByAddress(ptr);
		}
};

static Mempool g_pool;

void* mempool::allocate(size_t size, int align)
{
	printf("%d\n", size);
#if 0
	void *allocated_memory = ::malloc(size);
	assert(allocated_memory != 0);
	return allocated_memory;
#else
	return g_pool.allocate(size, align);
#endif
}

void mempool::free(void *ptr, size_t size)
{
#if 0
	::free(ptr);
#else
	g_pool.free(ptr, size);
#endif
}
