#ifndef __OBJECTPOOL_H_
#define __OBJECTPOOL_H_

#include <assert.h>

// Game Programming Gems 4
template <typename TYPE, int SIZE>
class BucketPool
{
protected:
	TYPE		m_Array[SIZE];
	TYPE		*m_Free[SIZE];
	int			m_Size;

public:

	BucketPool()
	{
		clear();
	}

	bool	full();
	bool	empty();
	void	clear();
	TYPE	*alloc();
	void	free(TYPE *elem);
	void	freeByAddress(void *address);
};


template <typename TYPE, int SIZE>
bool BucketPool<TYPE, SIZE>::full()
{
	return (m_Size >= SIZE);
}

template <typename TYPE, int SIZE>
bool BucketPool<TYPE, SIZE>::empty()
{
	return (m_Size <= 0);
}

template <typename TYPE, int SIZE>
void BucketPool<TYPE, SIZE>::clear()
{
	m_Size = 0;
	for (int i = 0; i < SIZE; i++)
	{
		m_Free[i] = &m_Array[i];
	}
}

template <typename TYPE, int SIZE>
TYPE* BucketPool<TYPE, SIZE>::alloc()
{
	assert(!full());
	m_Size++;
	return m_Free[m_Size - 1];
}

template <typename TYPE, int SIZE>
void BucketPool<TYPE, SIZE>::free(TYPE *elem)
{
	assert(!empty());
	m_Size--;
	m_Free[m_Size] = elem;
}

template <typename TYPE, int SIZE>
void BucketPool<TYPE, SIZE>::freeByAddress(void *address)
{
	assert(!empty());

	TYPE *elem = nullptr;
	for (int i = 0; i < SIZE; i++)
	{
		if (m_Array[i].getAddress() == address)
		{
			elem = &m_Array[i];
			break ;
		}
	}
	assert(elem);
	m_Size--;
	m_Free[m_Size] = elem;
}

#endif
