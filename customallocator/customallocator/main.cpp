#include <vector>
#include <map>
#include <list>
#include <cstddef>

#include <conio.h>

#include "stl_allocator.h"

struct data
{
	data()
	{
		int i = 0;
	}

	data(bool b)
	{
		int i = 0;
	}
	data(bool b, int x)
	{
		int i = 0;
	}

	~data()
	{
		int i = 0;
	}

	char array[32];
};

#define std_vector_decl(mytype, vectorname)		std::vector< mytype, Allocator<mytype> >														vectorname;
#define std_map_decl(keytype, mytype, mapname)	std::map< keytype, mytype, std::less<keytype>, Allocator<std::pair<keytype, mytype> > >			mapname;
#define std_list_decl(mytype, listname)			std::list< mytype, Allocator<mytype> >															listname;

void main()
{
	int max = 10000;
	int sum = 0;
	
	data *ptr1 = new_object<data>(true, 2014);
	delete_object(ptr1);

	for (int j = 0; j < 1; j++)
	{
		printf("start(%d)\n", j);
		#if 0
		{
			std_vector_decl(data, datavec);
			for (int i = 0; i < max; i++)
			{
				datavec.push_back(data());
			}
		}
		#endif
		#if 0
		{
			std_map_decl(int, data, datamap);
			for (int i = 0; i < max; i++)
			{
				datamap[i] = data();
			}
		}
		#endif

		#if 0
		{
			std_list_decl(data, datalist);
			for (int i = 0; i < max; i++)
			{
				datalist.push_back(data());
			}
		}
		#endif

		printf("alloc done \n");
	}

	printf("%d\n", sum);
	_getch();
}
